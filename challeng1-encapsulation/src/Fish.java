public class Fish {
    private String type;
    private String feed;

    public Fish(String type, String feed) {
        this.type = type;
        this.feed = feed;
    }

    public String getType() {
        return type;
    }

    public String getFeed() {
        return feed;
    }

    protected void show_identity() {
        System.out.printf("Saya Ikan dengan detail, Jenis: %s, makanan: %s", getType(), getFeed());
    }

}
