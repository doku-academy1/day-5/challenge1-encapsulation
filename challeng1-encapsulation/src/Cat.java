public class Cat {
    private String fur_color;
    private Integer num_of_leg;

    public Cat(String fur_color, Integer num_of_leg) {
        this.fur_color = fur_color;
        this.num_of_leg = num_of_leg;
    }

    public String getFur_color() {
        return fur_color;
    }

    public Integer getNum_of_leg() {
        return num_of_leg;
    }

    protected void show_identity(){
        System.out.printf("Saya Kucing dengan detail, Warna Bulu: %s dengan jumlah kaki: %d", getFur_color(), getNum_of_leg());
    }
}
