public class Car {
    private String type, color;
    private Integer num_of_tire;

    public Car(String type, String color, Integer num_of_tire) {
        this.type = type;
        this.color = color;
        this.num_of_tire = num_of_tire;
    }

    public String getType() {
        return type;
    }

    public String getColor() {
        return color;
    }

    public Integer getNum_of_tire() {
        return num_of_tire;
    }

    protected void show_identity() {
        System.out.printf("Saya mobil dengan detail, type: %s, color: %s, num of tire: %d", getType(), getColor(), getNum_of_tire());
    }

}
