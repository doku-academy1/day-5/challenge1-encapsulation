public class Flower {
    private String name, color;
    private Integer num_of_petal;

    public Flower(String name, String color, Integer num_of_petal) {
        this.name = name;
        this.color = color;
        this.num_of_petal = num_of_petal;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    public Integer getNum_of_petal() {
        return num_of_petal;
    }

    protected void show_identity() {
        System.out.printf("Saya Bunga dengan detail, Jenis: %s, color: %s, num of petal: %d", getName(), getColor(), getNum_of_petal());
    }
}
