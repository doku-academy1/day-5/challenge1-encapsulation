public class Main {
    public static void main(String[] args) {
        // define class cat
        Cat cat = new Cat("Hitam", 4);
        cat.show_identity();
        System.out.println();

        // define class fish
        Fish fish = new Fish("paus", "cacing");
        fish.show_identity();
        System.out.println();

        // define class flower
        Flower flower = new Flower("bangkai", "merah", 12);
        flower.show_identity();
        System.out.println();

        // define class car
        Car car = new Car("sedan", "merah", 4);
        car.show_identity();
    }
}